#!/bin/bash

# Generate a random number in the range 100000 to 999999
random_number=$((100000 + RANDOM % 900000))

# Replace the string "laki" with the random number in the specified file
sed -i "s/laki/$random_number/g" config.json
